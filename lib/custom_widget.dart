import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';
import 'package:hackathon/card_button.dart';

class CustomWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CardButton(
      icon: Icons.payment,
      size: 50,
      text: "Payback",
      onTap: () {}
    );
  }
}
