import 'package:flutter_web/material.dart';
import 'package:hackathon/custom_widget.dart';
import 'package:hackathon/flutter_screenutil.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    double baseHeight = 640.0;
    double baseWidth = 480.0;
    ScreenUtil.instance = ScreenUtil(width:baseWidth, height: baseHeight, allowFontScaling: true)..init(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            left: ScreenUtil.getInstance().setWidth(240),
            child: Container(
              height: ScreenUtil.getInstance().setHeight(640),
              width: 1,
              decoration: BoxDecoration(
                border: Border(left: BorderSide(color: Theme.of(context).dividerColor))
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil.getInstance().setHeight(50),
            left: 0,
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil.getInstance().setWidth(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Hello, World Hello!',
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil.getInstance().setHeight(50),
            left: ScreenUtil.getInstance().setWidth(240),
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil.getInstance().setWidth(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CustomWidget(),
                ],
              ),
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
