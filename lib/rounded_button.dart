import 'package:flutter_web/material.dart';

class RoundedButton extends StatefulWidget {
  final Color color;
  final Widget child;
  double borderWidth;
  bool filled;
  Function callbackFunction = () => {};

  RoundedButton(
    {this.color,
      this.child,
      this.filled = true,
      this.borderWidth = 1,
      this.callbackFunction});

  @override
  _RoundedButtonState createState() => _RoundedButtonState();
}

class _RoundedButtonState extends State<RoundedButton> {
  Widget _flatButton() {
    return FlatButton(
      onPressed: () => widget.callbackFunction(),
      child: widget.child,
      textColor: widget.color,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: widget.color, width: widget.borderWidth, style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(15)),
    );
  }

  Widget _raisedButton() {
    return RaisedButton(
      elevation: 0.0,
      highlightElevation: 0.0,
      disabledElevation: 0.0,
      onPressed: () => widget.callbackFunction(),
      child: widget.child,
      color: widget.color,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: widget.color, width: widget.borderWidth, style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(15)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.filled ? _raisedButton() : _flatButton();
  }
}
