import 'package:flutter_web/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        title: 'Flutter widgets catalog',
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    return MyHomePageState(this.title);
  }
}

class MyHomePageState extends State<MyHomePage> {
  final String title;

  final TextEditingController _filter = new TextEditingController();
  String _searchText = "";

  List<String> widgets = new List();
  List<String> filteredWidgets = new List();

  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Flutter widgets catalog');

  List<Widget> widgetList;

  bool startPage = true;

  MyHomePageState(this.title) {

  }

  @override
  void initState() {
    widgets.add("TextField with icon");
    widgets.add("Animation for tab bar");
    widgets.add("Beautifull stack version");
    widgets.add("Blur effect container");
    widgets.add("Blur effect container 2.0");

    widgets.add("Custom font");
    widgets.add("New way use column and row");
    widgets.add("New way use column and row 2.0");
    widgets.add("Padding with features");

    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredWidgets = widgets;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
    super.initState();
  }

  _searchPressed() {
    startPage = false;
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Flutter widgets catalog');
        filteredWidgets = widgets;
        _filter.clear();
      }
    });
  }

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List<String> tempList = new List();
      for (int i = 0; i < filteredWidgets.length; i++) {
        if (filteredWidgets[i]
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredWidgets[i]);
        }
      }
      filteredWidgets = tempList;
    }
    return ListView.builder(
      itemCount: widgets == null ? 0 : filteredWidgets.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            height: 50,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5)),
            child: InkWell(
                child: Center(
              child: Text(filteredWidgets[index]),
            ),
            // onTap: ,
            ));
      },
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  Widget _body(){
    if (startPage){
      return Container(
        child: Center(
          child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             new Stack(
              alignment: Alignment.center,
              children: <Widget>[
                new Container(
                  height: 100.0,
                  width: 100.0,
                  decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(50.0),
                      color: Color(0xFF18D191)),
                  child: new Icon(
                    Icons.local_offer,
                    color: Colors.white,
                  ),
                ),
                new Container(
                  margin: new EdgeInsets.only(right: 50.0, top: 50.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(50.0),
                      color: Color(0xFFFC6A7F)),
                  child: new Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                ),
                new Container(
                  margin: new EdgeInsets.only(left: 30.0, top: 50.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(50.0),
                      color: Color(0xFFFFCE56)),
                  child: new Icon(
                    Icons.local_car_wash,
                    color: Colors.white,
                  ),
                ),
                new Container(
                  margin: new EdgeInsets.only(left: 90.0, top: 40.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(50.0),
                      color: Color(0xFF45E0EC)),
                  child: new Icon(
                    Icons.notifications_active,
                    color: Colors.white,
                    size: 40,
                  ),
                )
              ]
             )
          ]
          )
        )
      );
    } else{
      return _buildList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _body(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }
}
