import 'package:flutter_web/material.dart';
import 'package:hackathon/flutter_screenutil.dart';

class CardButton extends StatelessWidget {
  final IconData icon;
  final Widget image;
  final String text;
  final double size;
  final Function onTap;

  CardButton({this.icon, this.image, this.size, this.text, this.onTap}) :
      assert(image != null || icon != null),
      assert(size != null);


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Material(
          borderRadius: BorderRadius.circular(14),
          shadowColor: Colors.grey.withOpacity(0.6),
          elevation: 4,
          child: InkWell(
            borderRadius: BorderRadius.circular(14),
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: icon != null ? Icon(icon, size: this.size, color: Color.fromARGB(255, 241, 86, 86)) : image,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: this.size / 5),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(fontFamily: 'Montserrat-Regular', fontSize: ScreenUtil.getInstance().setSp(15)),
          ),
        )
      ],
    );
  }
}
